package com.company.GUI;

import com.company.Accounts.Account;
import com.company.Accounts.DepositException;
import com.company.Accounts.SavingAccount;
import com.company.Accounts.WithdrawalException;
import com.company.Bank.AddPersonException;
import com.company.Bank.HolderException;
import com.company.Bank.RemovePersonException;
import com.company.Person.Person;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Adi on 15-May-17.
 */
public class Controller {

    private MainView mainView;
    private AccountsView accountsView;

    private Model model;

    private SelectionErrorView selectionErrorView;
    private Error error;

    private AddClientView addClientView;
    private EditClientView editClientView;
    private RemoveClientView removeClientView;

    private SpendingAccountView spendingAccountView;
    private SavingAccountView savingAccountView;
    private RemoveAccountView removeAccountView;
    private WithdrawMoneyView withdrawMoneyView;
    private DepositMoneyView depositMoneyView;

    public Controller(MainView mainView, Model model) {

        this.mainView = mainView;
        this.model = model;

        mainView.addAddClientListener(new AddClient());
        mainView.addEditClientListener(new EditClientListener());
        mainView.addRemoveClientListener(new RemoveClientListener());
        mainView.addAccountsListener(new ViewAccountsListener());

        mainView.addTableClickListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                int row = mainView.getRowSelected();

                if (row == -1) {

                    selectionErrorView = new SelectionErrorView();

                } else {

                    Person person = new Person((String) mainView.getValueFromTable(row, 0), (String) mainView.getValueFromTable(row, 1),
                            (double) mainView.getValueFromTable(row, 2), (double) mainView.getValueFromTable(row, 3));

                    accountsView = new AccountsView(person, model);

                    accountsView.addAddSpendingAccountListener(new AddSpendingAccountListener());
                    accountsView.addAddSavingAccountListener(new AddSavingAccountListener());
                    accountsView.addRemoveAccountListener(new RemoveAccountListener());
                    accountsView.addWithdrawListener(new WithdrawMoneyListener());
                    accountsView.addDepositListener(new DepositMoneyListener());

                }

            }
        });

    }


    public static JTable createTable(Set<Person> people) {

        JTable table = null;

        String[] fields = {"Name", "Email", "Withdrawn Amount", "Deposited Amoun"};
        Object[][] data = new Object[people.size()][fields.length];

        if (!(people.isEmpty())) {

            int i = 0;

            for (Person person : people) {

                data[i][0] = person.getName();
                data[i][1] = person.getEmail();
                data[i][2] = person.getWithdrawnAmount();
                data[i][3] = person.getDepositedAmount();

                i++;

            }

        }

        table = new JTable(data, fields);

        return table;

    }

    public static JTable createTable(List<Account> accounts) {

        JTable table = null;

        String[] fields = {"ID", "Type", "Money", "Interest"};

        Object[][] data = new Object[accounts.size()][fields.length];

        if (!(accounts.isEmpty())) {

            for (int i = 0; i < accounts.size(); i++) {

                Class accountClass = accounts.get(i).getClass();

                data[i][0] = accounts.get(i).getId();
                data[i][1] = accountClass.getSimpleName();
                data[i][2] = accounts.get(i).getMoney();
                data[i][3] = accounts.get(i) instanceof SavingAccount ? ((SavingAccount) accounts.get(i)).getInterest() : "No interest";

            }

        }

        table = new JTable(data, fields);

        return table;

    }

    private class OkDepositMoneyListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            int row = accountsView.getRowSelected();

            try {

                model.depositMoney(accountsView.getPerson(), (UUID)accountsView.getValueFromTable(row, 0),
                        depositMoneyView.getMoney());




            } catch (DepositException e1) {
                error = new Error(e1.getMessage());
             }catch(NumberFormatException e2){
                error = new Error(e2.getMessage());
            }finally{

                accountsView.updateTable();
                mainView.updateTable();
                depositMoneyView.dispose();

            }

        }
    }

    public class DepositMoneyListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            int row = accountsView.getRowSelected();

            if(row == -1){

                selectionErrorView = new SelectionErrorView();

            }else{

                depositMoneyView = new DepositMoneyView();

                depositMoneyView.addOkListener(new OkDepositMoneyListener());

            }

        }
    }

    public class OkWithdrawMoneyListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            int row = accountsView.getRowSelected();

            try {

                model.withdrawMoney(accountsView.getPerson(), (UUID)accountsView.getValueFromTable(row, 0),
                        withdrawMoneyView.getMoney());

                accountsView.updateTable();
                mainView.updateTable();
                withdrawMoneyView.dispose();


            } catch (WithdrawalException e1) {
                error = new Error(e1.getMessage());
            }catch(NumberFormatException e2 ){
                error = new Error(e2.getMessage());
            }finally{

                accountsView.updateTable();
                mainView.updateTable();
                withdrawMoneyView.dispose();

            }


        }
    }

    public class WithdrawMoneyListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            int row = accountsView.getRowSelected();

            if(row == -1){

                selectionErrorView = new SelectionErrorView();

            }else {

                withdrawMoneyView = new WithdrawMoneyView();

                withdrawMoneyView.addOkListener(new OkWithdrawMoneyListener());
            }

        }
    }

    public class OkRemoveAccountListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            int row = accountsView.getRowSelected();

            try {

                model.removeHolderAssociatedAccount(accountsView.getPerson(), (UUID)accountsView.getValueFromTable(row, 0));



            } catch (HolderException e1) {
                error = new Error(e1.getMessage());
            }catch (NumberFormatException e2){
                error = new Error(e2.getMessage());
            }finally{

                removeAccountView.dispose();
                accountsView.updateTable();

            }

        }
    }

    public class RemoveAccountListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            int row = accountsView.getRowSelected();

            if(row == -1){

                selectionErrorView = new SelectionErrorView();

            }else{

                removeAccountView = new RemoveAccountView();

                removeAccountView.addOkListener(new OkRemoveAccountListener());

            }

        }
    }

    public class OkSavingAccountListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            try {

                model.addSavingAccount(accountsView.getPerson(), savingAccountView.getMoney());



            } catch (HolderException e1) {
                error = new Error(e1.getMessage());
            }finally{

                savingAccountView.dispose();

                accountsView.updateTable();

            }

        }
    }

    public class AddSavingAccountListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            savingAccountView = new SavingAccountView();

            savingAccountView.addOkListener(new OkSavingAccountListener());

        }
    }

    public class OkSpendingAccountListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            try {

                model.addSpendingAccount(spendingAccountView.getMoney(), accountsView.getPerson());



            } catch (HolderException e1) {
                error = new Error(e1.getMessage());
            }finally{


                accountsView.updateTable();

                spendingAccountView.dispose();
            }

        }
    }

    public class AddSpendingAccountListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            spendingAccountView = new SpendingAccountView();

            spendingAccountView.addOkListener(new OkSpendingAccountListener());

        }
    }


    public class OkRemoveClientListener implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {
            int row = mainView.getRowSelected();

            Person p = new Person((String) mainView.getValueFromTable(row, 0), (String) mainView.getValueFromTable(row, 1));

            try {

                model.removePerson(p);



            } catch (RemovePersonException e1) {

                error = new Error(e1.getMessage());

            }finally{

                mainView.updateTable();

                removeClientView.dispose();

            }

        }
    }

    public class ViewAccountsListener implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {

            int row = mainView.getRowSelected();

            if (row == -1) {

                selectionErrorView = new SelectionErrorView();

            } else {

                Person person = new Person((String) mainView.getValueFromTable(row, 0), (String) mainView.getValueFromTable(row, 1),
                        (double) mainView.getValueFromTable(row, 2), (double) mainView.getValueFromTable(row, 3));

                accountsView = new AccountsView(person, model);

                accountsView.addAddSpendingAccountListener(new AddSpendingAccountListener());
                accountsView.addAddSavingAccountListener(new AddSavingAccountListener());
                accountsView.addRemoveAccountListener(new RemoveAccountListener());
                accountsView.addWithdrawListener(new WithdrawMoneyListener());
                accountsView.addDepositListener(new DepositMoneyListener());

            }

        }
    }

    public class RemoveClientListener implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {

            int row = mainView.getRowSelected();

            if (row == -1) {

                selectionErrorView = new SelectionErrorView();

            } else {

                removeClientView = new RemoveClientView();

                removeClientView.addOkListener(new OkRemoveClientListener());

            }

        }
    }


    public class OkEditClientListener implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {


            Person person2 = new Person(editClientView.getName(), editClientView.getEmail(),
                    editClientView.getPerson().getWithdrawnAmount(), editClientView.getPerson().getDepositedAmount());

            model.editPerson(editClientView.getPerson(), person2);

            editClientView.dispose();

            mainView.updateTable();

        }
    }

    public class EditClientListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            int row = mainView.getRowSelected();

            if (row == -1) {

                selectionErrorView = new SelectionErrorView();

            } else {

                Person person = new Person((String) mainView.getValueFromTable(row, 0), (String) mainView.getValueFromTable(row, 1),
                        (double) mainView.getValueFromTable(row, 2), (double) mainView.getValueFromTable(row, 3));

                editClientView = new EditClientView(person.getName(), person.getEmail(), person);

                editClientView.addOkListener(new OkEditClientListener());
            }

        }
    }

    public class AddClient implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {

            addClientView = new AddClientView();

            addClientView.addOkListener(new OkAddClient());
            addClientView.addCancelListener(new CancelAddClient());

        }
    }

    public class OkAddClient implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            try {

                model.addPerson(addClientView.getName(), addClientView.getEmail());


            } catch (AddPersonException e1) {

                error = new Error(e1.getMessage());

            } finally {

                addClientView.dispose();

                mainView.updateTable();

            }

        }
    }

    public class CancelAddClient implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            addClientView.dispose();

        }
    }

}
