package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 15-May-17.
 */
public class AddClientView {

    JFrame frame;

    private JButton ok = new JButton("Ok");
    private JButton cancel = new JButton("Cancel");

    private JTextField name = new JTextField(40);
    private JTextField email = new JTextField(40);

    private JPanel panel = new JPanel();

    public AddClientView(){

        frame = new JFrame("Add Client");

        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720, 360);

        JLabel n = new JLabel("Name: ");
        JLabel e =  new JLabel("Email: ");

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();

        p1.add(n);
        p1.add(name);
        p2.add(e);
        p2.add(email);
        p3.add(ok);
        p3.add(cancel);

        panel.add(p1);
        panel.add(p2);
        panel.add(p3);

        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        frame.setContentPane(panel);
        frame.setVisible(true);

    }

    public String getName(){

        return name.getText();

    }

    public String getEmail(){

        return email.getText();

    }

    public void dispose(){

        frame.dispose();

    }

    public void addCancelListener(ActionListener cl){

        cancel.addActionListener(cl);

    }

    public void addOkListener(ActionListener ol){

        ok.addActionListener(ol);

    }




}
