package com.company.GUI;

import com.company.Accounts.*;
import com.company.Bank.AddPersonException;
import com.company.Bank.Bank;
import com.company.Bank.HolderException;
import com.company.Bank.RemovePersonException;
import com.company.Person.Person;

import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Adi on 15-May-17.
 */
public class Model {

    private Bank bank;

    public Model() {

        this.bank = new Bank();

    }

    public List<Account> getAccounts(Person person){

        return bank.getAccounts(person);

    }

    public void addSpendingAccount(double money, Person person) throws HolderException {

        bank.addHolderAssociatedAccount(person, new SpendingAccount(money));

    }

    public void addSavingAccount(Person person, double money) throws HolderException{

        bank.addHolderAssociatedAccount(person, new SavingAccount(money));

    }

    public double withdrawAllMoney(Person person, Account account) throws WithdrawalException {

        return bank.withdrawAllMoney(person, account);

    }

    public double withdrawMoney(Person person, UUID id, double money) throws WithdrawalException {

        Account account = new Account(id);

        return bank.withdrawMoney(person, account, money);

    }

    public void depositMoney(Person person, UUID id, double money) throws DepositException {

        Account account = new Account(id);

        bank.depositMoney(person, account, money);

    }

    public void addPerson(String name, String email) throws AddPersonException {

        bank.addPerson(new Person(name, email));

    }

    public void removePerson(Person person) throws RemovePersonException {

        bank.removePerson(person);

    }

    public void addHolderAssociatedAccount(Person person, Account account) throws HolderException {

        bank.addHolderAssociatedAccount(person, account);

    }

    public void removeHolderAssociatedAccount(Person person, UUID id) throws HolderException {

        Account account = new Account(id);

        bank.removeHolderAssociatedAccount(person, account);

    }

    public void writeAccountData(){

        bank.writeAccountData();

    }

    public Set<Person> getPeople(){

        return bank.people();

    }

    public void editPerson(Person person, Person person2){

        bank.editPerson(person, person2);

    }


}
