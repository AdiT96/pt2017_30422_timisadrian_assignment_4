package com.company.GUI;

import javax.swing.*;
import java.awt.event.*;

/**
 * Created by Adi on 15-May-17.
 */
public class MainView {

    private JFrame frame;

    private JTable table;
    private JScrollPane scroll;

    private JButton addPerson = new JButton("Add Client");
    private JButton editPerson = new JButton("Edit Client");
    private JButton removePerson = new JButton("Remove Client");
    private JButton accounts = new JButton("View Accounts");

    private JPanel p;


    private Model model;

    public MainView(Model model){

        this.model = model;

        frame = new JFrame("Bank");

        frame.setSize(1080, 720);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {

                model.writeAccountData();

                frame.dispose();

                super.windowClosing(e);
            }
        });

        table = Controller.createTable(model.getPeople());

        p = new JPanel();

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();

        scroll = new JScrollPane(table);
        p1.add(scroll);

        p2.add(addPerson);
        p2.add(editPerson);
        p2.add(removePerson);
        p2.add(accounts);

        p.add(p1);
        p.add(p2);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        frame.setContentPane(p);
        frame.setVisible(true);

    }

    public void updateTable(){

        scroll.getViewport().remove(table);
        table = Controller.createTable(model.getPeople());
        scroll.getViewport().add(table);

    }

    public int getRowSelected(){

        return table.getSelectedRow();

    }

    public Object getValueFromTable(int row, int column){

        return table.getModel().getValueAt(row, column);

    }

    public void addAccountsListener(ActionListener al){

        accounts.addActionListener(al);

    }

    public void addAddClientListener(ActionListener ac){

        addPerson.addActionListener(ac);

    }

    public void addEditClientListener(ActionListener el){

        editPerson.addActionListener(el);

    }

    public void addRemoveClientListener(ActionListener rl){

        removePerson.addActionListener(rl);

    }

    public void addTableClickListener(MouseAdapter ml){

        table.addMouseListener(ml);

    }



}
