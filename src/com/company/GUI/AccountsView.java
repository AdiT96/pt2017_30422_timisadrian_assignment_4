package com.company.GUI;

import com.company.Person.Person;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 15-May-17.
 */
public class AccountsView {

    private Person person;
    private Model model;

    private JFrame frame;

    private JTable table;
    private JScrollPane scroll;

    private JButton addSpendingAccount = new JButton("Add Spending Account");
    private JButton addSavingAccount = new JButton("Add Saving Account");
    private JButton removeAccount = new JButton("Remove Account");
    private JButton withdrawMoney = new JButton("Withdraw Money");
    private JButton depositMoney = new JButton("Deposit Money");

    private JButton back = new JButton("Back");

    private JPanel p;

    public AccountsView(Person person, Model model){

        this.person = person;
        this.model = model;

        frame = new JFrame("Accounts");

        frame.setSize(1080, 720);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        table = Controller.createTable(model.getAccounts(this.person));

        p = new JPanel();

        JPanel p0 = new JPanel();
        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();

        p0.add(back);

        scroll = new JScrollPane(table);
        p1.add(scroll);

        p2.add(addSpendingAccount);
        p2.add(addSavingAccount);
        p2.add(removeAccount);
        p2.add(withdrawMoney);
        p2.add(depositMoney);

        p.add(p0);
        p.add(p1);
        p.add(p2);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        frame.setContentPane(p);
        frame.setVisible(true);

        back.addActionListener(new BackActionListener());

    }

    public Person getPerson() {
        return person;
    }

    public void updateTable(){

        scroll.getViewport().remove(table);
        table = Controller.createTable(model.getAccounts(this.person));
        scroll.getViewport().add(table);

    }

    public int getRowSelected(){

        return table.getSelectedRow();

    }

    public Object getValueFromTable(int row, int column){

        return table.getModel().getValueAt(row, column);

    }

    public class BackActionListener implements ActionListener{


        @Override
        public void actionPerformed(ActionEvent e) {
            dispose();
        }
    }

    public void addWithdrawListener(ActionListener wl){

        withdrawMoney.addActionListener(wl);

    }

    public void addDepositListener(ActionListener dl){

        depositMoney.addActionListener(dl);

    }

    public void addBackListener(ActionListener bl){

        back.addActionListener(bl);

    }

    public void addAddSpendingAccountListener(ActionListener ac){

        addSpendingAccount.addActionListener(ac);

    }

    public void addAddSavingAccountListener(ActionListener asl){

        addSavingAccount.addActionListener(asl);

    }

    public void addRemoveAccountListener(ActionListener rl){

        removeAccount.addActionListener(rl);

    }

    public void dispose(){

        frame.dispose();

    }


}
