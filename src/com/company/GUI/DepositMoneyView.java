package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 15-May-17.
 */
public class DepositMoneyView {

    private JFrame frame;

    private JPanel panel;

    private JButton cancel = new JButton("Cancel");
    private JButton ok = new JButton("Ok");

    private JTextField money = new JTextField(40);

    public DepositMoneyView() {

        frame = new JFrame("Deposit Money");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720, 360);

        JLabel m = new JLabel("Money: ");

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();

        p1.add(m);
        p1.add(money);
        p2.add(ok);
        p2.add(cancel);

        panel = new JPanel();

        panel.add(p1);
        panel.add(p2);

        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        frame.setContentPane(panel);
        frame.setVisible(true);

        cancel.addActionListener(new CancelListener());

    }

    public class CancelListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            dispose();
        }
    }

    public double getMoney() throws NumberFormatException{

        return Double.parseDouble(money.getText());

    }

    public void dispose() {

        frame.dispose();

    }

    public void addCancelListener(ActionListener cl) {

        cancel.addActionListener(cl);

    }

    public void addOkListener(ActionListener ol) {

        ok.addActionListener(ol);

    }

}
