package com.company;
import com.company.Accounts.Account;
import com.company.Accounts.SavingAccount;
import com.company.Accounts.SpendingAccount;
import com.company.Bank.AddPersonException;
import com.company.Bank.Bank;
import com.company.Bank.HolderException;
import com.company.Bank.RemovePersonException;
import com.company.GUI.Controller;
import com.company.GUI.MainView;
import com.company.GUI.Model;
import com.company.Person.Person;

import java.io.*;

public class Main {

    public static void main(String[] args) {

        Model model = new Model();
        MainView mainView = new MainView(model);
        Controller controler = new Controller(mainView, model);

    }
}
