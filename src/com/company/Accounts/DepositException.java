package com.company.Accounts;

/**
 * Created by Adi on 14-May-17.
 */
public class DepositException extends Exception {

    public DepositException(String message){

        super(message);

    }

}
