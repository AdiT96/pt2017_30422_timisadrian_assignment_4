package com.company.Accounts;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.UUID;

/**
 * Created by Adi on 29-Apr-17.
 */
public class SavingAccount extends Account implements Serializable {

    private double interest;
    private boolean active;

    public SavingAccount(UUID id){
        super(id);
        active = false;
    }

    public SavingAccount(){

        super();

        active = false;
        this.interest = 0;

    }

    public SavingAccount(double money){

        super(money);

        active = true;

        this.interest = (6.0 /100) * this.money;

    }

    public double getInterest() {
        return interest;
    }

    public void depositMoney(double money) throws DepositException{

        if(!this.active){

            if(money <= 0){

                throw new DepositException("You cannot deposit negative sums of money");

            }else{

                this.money = money;

                this.interest = (6.0 /100) * this.money;

            }

        }else{

            //System.out.println("You can deposit money only once in the saving account!");

            throw new DepositException("You can deposit money only once in the savings account");

        }

    }

    public double withdrawMoney() throws WithdrawalException{

        if (this.active) {


            double returned = super.withdrawMoney();

            this.interest = 0;

            return returned;
        }else{

            throw new WithdrawalException("The account is not active");

        }

    }

    public String toString(){

        DecimalFormat df = new DecimalFormat("#.00");

        return "The sum available in the account: " + df.format(money) + "\n" +
                "With the interest: " + df.format(interest);

    }



}


