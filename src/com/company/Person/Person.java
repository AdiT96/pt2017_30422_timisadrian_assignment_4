package com.company.Person;

import com.company.Bank.Bank;
import com.company.Bank.Update;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Adi on 29-Apr-17.
 */


public class Person implements Serializable, Observer{

    private String name;
    private String email;
    private double withdrawnAmount;
    private double depositedAmount;

    public Person(String name, String email){

        this.name = name;
        this.email = email;
        this.withdrawnAmount = 0;
        this.depositedAmount = 0;

    }

    public Person(String name, String email, double withdrawnAmount, double depositedAmount) {
        this.name = name;
        this.email = email;
        this.withdrawnAmount = withdrawnAmount;
        this.depositedAmount = depositedAmount;
    }

    public void edit(String name, String email){

        this.name = name;
        this.email = email;

    }

    public String getName(){

        return this.name;

    }

    public String getEmail(){

        return this.email;

    }

    public double getWithdrawnAmount() {
        return withdrawnAmount;
    }

    public double getDepositedAmount() {
        return depositedAmount;
    }

    public int hashCode(){

        int intValue = 0;

        for(int i = 0; i < name.length(); i++){

            intValue += (int)name.charAt(i);

        }

        return intValue;

    }

    @Override
    public boolean equals(Object obj) {

        if(obj instanceof Person) {

            return name.equals(((Person) obj).getName());
        }else
            return false;

    }

    @Override
    public void update(Observable o, Object arg) {

        if(arg instanceof Update){

            if(((Update) arg).getPersonToBeUpdated().equals(this)){

                this.depositedAmount += ((Update) arg).getMoneyDeposited();
                this.withdrawnAmount += ((Update) arg).getMoneyWithdrawn();

            }

        }

    }

    public String toString(){

        return name + "  " + email + "  " + withdrawnAmount + "   " + depositedAmount;

    }



}
