package com.company.Bank;

import com.company.Accounts.Account;
import com.company.Accounts.DepositException;
import com.company.Accounts.SavingAccount;
import com.company.Accounts.WithdrawalException;
import com.company.Person.Person;

import java.io.*;
import java.util.*;

/**
 * Created by Adi on 29-Apr-17.
 */

public class Bank extends Observable implements BankProc {

    private HashMap<Person, List<Account>> personAccounts;

    public Bank() {

        personAccounts = new HashMap<>();

        readAccountData();

    }

    public Set<Person> people() {

        return personAccounts.keySet();

    }

    private boolean isWellFormed() {

        Set<Person> people = personAccounts.keySet();

        int n = 0;

        for (Person person : people) {

            if (!personAccounts.containsKey(person)) {

                return false;

            }

            n++;

            if (!(personAccounts.get(person) instanceof ArrayList)) {

                return false;

            }

        }

        return n == people.size();

    }

    public List<Account> getAccounts(Person person) {

        return personAccounts.get(person);

    }

    public void editPerson(Person person, Person person2) {

        if (personAccounts.keySet().contains(person)) {

            List<Account> aux = personAccounts.get(person);

            personAccounts.remove(person);

            personAccounts.put(person2, aux);

        }

    }

    @Override
    public double withdrawAllMoney(Person person, Account account) throws WithdrawalException {

        assert person != null;
        assert account != null;
        assert isWellFormed();

        int index = personAccounts.get(person).indexOf(account);

        double moneyWithdrawn = personAccounts.get(person).get(index).withdrawMoney();

        Update update = new Update(person, moneyWithdrawn, 0);

        notifyAllObservers(update);

        personAccounts.get(person).remove(index);

        assert personAccounts.get(person).get(index).getMoney() == 0;
        assert isWellFormed();

        return moneyWithdrawn;

    }

    @Override
    public double withdrawMoney(Person person, Account account, double money) throws WithdrawalException {

        assert person != null;
        assert account != null;
        assert money > 0;
        assert isWellFormed();

        if(money < 0){
            throw new WithdrawalException("You cannot withdraw negative sums of money");
        }

        int index = personAccounts.get(person).indexOf(account);

        if (personAccounts.get(person).get(index) instanceof SavingAccount) {
            return withdrawAllMoney(person, account);
        } else {

            double initialMoney = personAccounts.get(person).get(index).getMoney();

            double moneyWithdrawn = personAccounts.get(person).get(index).withdrawMoney(money);

            Update update = new Update(person, money, 0);

            notifyAllObservers(update);

            assert initialMoney - money == personAccounts.get(person).get(index).getMoney();
            assert isWellFormed();

            return moneyWithdrawn;
        }

    }

    @Override
    public void depositMoney(Person person, Account account, double money) throws DepositException {

        assert person != null;
        assert account != null;
        assert money > 0;
        assert isWellFormed();


        if(money < 0){
            throw new DepositException("You cannot deposit negative sums of money");
        }

        int index = personAccounts.get(person).indexOf(account);

        double initialMoney = personAccounts.get(person).get(index).getMoney();

        personAccounts.get(person).get(index).depositMoney(money);

        Update update = new Update(person, 0, money);

        notifyAllObservers(update);

        assert initialMoney + money == personAccounts.get(person).get(index).getMoney();
        assert isWellFormed();

    }

    @Override
    public void addPerson(Person person) throws AddPersonException {

        assert person != null;
        assert (!(personAccounts.keySet().contains(person)));
        assert isWellFormed();

        if(personAccounts.keySet().contains(person)){
            throw new AddPersonException("This person is already in the bank");
        }

        int initialSize = personAccounts.keySet().size();

        personAccounts.put(person, new ArrayList<Account>());
        addObserver(person);

        assert initialSize + 1 == personAccounts.keySet().size();
        assert isWellFormed();

    }

    @Override
    public void removePerson(Person person) throws RemovePersonException {

        assert person != null;
        assert personAccounts.keySet().contains(person);
        assert isWellFormed();

        if(!personAccounts.keySet().contains(person)){
            throw new RemovePersonException("This person is not in the bank");
        }

        int initialSize = personAccounts.keySet().size();

        personAccounts.remove(person);
        deleteObserver(person);

        assert initialSize - 1 == personAccounts.keySet().size();
        assert personAccounts.get(person) == null;
        assert isWellFormed();

        deleteObserver(person);

    }

    @Override
    public void addHolderAssociatedAccount(Person person, Account account) throws HolderException {

        assert person != null;
        assert account != null;
        assert isWellFormed();


        if (person == null) {

            throw new HolderException("You cannot add a null person");

        } else if (account == null) {

            throw new HolderException("You cannot add a null account");

        } else {

            if (personAccounts.containsKey(person)) {

                int initialSize = personAccounts.get(person).size();

                personAccounts.get(person).add(account);

                assert initialSize + 1 == personAccounts.get(person).size();

            } else {

                personAccounts.put(person, new ArrayList<>());

                personAccounts.get(person).add(account);

                assert personAccounts.get(person) != null;
                assert personAccounts.get(person).size() == 1;

            }
        }

        assert isWellFormed();

    }

    @Override
    public void removeHolderAssociatedAccount(Person person, Account account) throws HolderException {

        assert person != null;
        assert account != null;
        assert isWellFormed();

        if (personAccounts.containsKey(person)) {

            int initialSize = personAccounts.get(person).size();

            personAccounts.get(person).remove(account);

            assert initialSize - 1 == personAccounts.get(person).size();


        } else {

            //System.out.println("The person has no associated accounts");

            throw new HolderException("The person has no associated accounts");

        }

        assert isWellFormed();

    }

    @Override
    public void readAccountData() {

        try {

            FileInputStream fin = new FileInputStream("bank.ser");
            ObjectInputStream in = new ObjectInputStream(fin);

            this.personAccounts = (HashMap) in.readObject();

            for (Person person : personAccounts.keySet()) {

                addObserver(person);

            }

            //fin.close();
            //in.close();

        } catch (EOFException e) {

            System.out.println(personAccounts.keySet().size());

            //System.out.println("Do nothing");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void writeAccountData() {

        try {

            FileOutputStream fout = new FileOutputStream("bank.ser");
            ObjectOutputStream out = new ObjectOutputStream(fout);

            out.writeObject(personAccounts);

            //fout.close();
            //out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void print() {

        Set<Person> oameni = personAccounts.keySet();

        for (Person person : oameni) {

            System.out.println("Omu : ");
            System.out.println(person);

            System.out.println("The accounts of the person");

            for (Account account : personAccounts.get(person)) {

                System.out.println(account);

            }


        }

    }

    public void notifyAllObservers(Update update) {

        for (Person person : personAccounts.keySet()) {

            person.update(this, update);

        }

    }

}
